package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.image.ImageView;

public class PieceQueen extends Piece {

	public PieceQueen(int type) {
		super(type);
		if (type == 1) {
			image = new ImageView("chess-pieces/W-Queen.png");
			getChildren().addAll(image);
		} else {
			image = new ImageView("chess-pieces/B-Queen.png");
			getChildren().addAll(image);
		}
	}

	public int getType() {
		return _Queen;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		if (_pieces[_x][_y] != null && _pieces[_x][_y].getColor() == _pieces[_pos_x][_pos_y].getColor())
			return false;

		boolean toReturn = true;

		// Bishop-like
		int diff_x = _pos_x - _x;
		int diff_y = _pos_y - _y;

		if (diff_x + diff_y != 0 && diff_x - diff_y != 0)
			toReturn = false;

		if (toReturn) {
			int it_x;
			int it_y;

			if (_x > _pos_x)
				it_x = 1;
			else
				it_x = -1;

			if (_y > _pos_y)
				it_y = 1;
			else
				it_y = -1;

			int x = _pos_x + it_x;
			int y = _pos_y + it_y;

			while (x != _x) {
				if (_pieces[x][y] != null) {
					toReturn = false;
					break;
				}

				x += it_x;
				y += it_y;
			}
		}

		if (toReturn)
			return true;

		// Rook-like
		if (_x == _pos_x) {
			int yMin;
			int yMax;

			if (_y > _pos_y) {
				yMax = _y;
				yMin = _pos_y;
			} else {
				yMax = _pos_y;
				yMin = _y;
			}

			for (int i_y = yMin + 1; i_y < yMax; i_y++) {
				if (_pieces[_x][i_y] != null)
					return false;
			}

			return true;
		} else if (_y == _pos_y) {
			int xMin;
			int xMax;

			if (_x > _pos_x) {
				xMax = _x;
				xMin = _pos_x;
			} else {
				xMax = _pos_x;
				xMin = _x;
			}

			for (int i_x = xMin + 1; i_x < xMax; i_x++) {
				if (_pieces[i_x][_y] != null)
					return false;
			}

			return true;
		}

		return false;
	}

	// the move method of Queen could utilize both the move method of the rook
	// and the bishop

}

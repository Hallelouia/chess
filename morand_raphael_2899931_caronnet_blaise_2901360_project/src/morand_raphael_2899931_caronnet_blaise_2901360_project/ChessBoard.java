package morand_raphael_2899931_caronnet_blaise_2901360_project;

import java.util.function.Predicate;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.util.Pair;

public class ChessBoard extends Pane {

	private int _boardHeight = 8;
	private int _boardWidth = 8;

	private GameLogic gl;

	private int[][] chessBoard;
	private Piece[][] pieces;
	private Window[][] windows;

	private double cell_width;
	private double cell_height;
	private int current_player;

	private final int _Empty = 0;
	private final int _Pawn = 1;
	private final int _Knight = 2;
	private final int _Bishop = 3;
	private final int _Rook = 4;
	private final int _Queen = 5;
	private final int _King = 6;

	private final int PLAYERWHITE = 1;
	private final int PLAYERBLACK = 2;

	private Boolean hasSelected = false;

	private Pair<Integer, Integer> selected;
	private String lastStatus;

	public ChessBoard() {

		// initalize the board: background, data structures, inital layout of
		// pieces
		chessBoard = new int[_boardWidth][_boardHeight];
		windows = new Window[_boardWidth][_boardHeight];
		pieces = new Piece[_boardWidth][_boardHeight];

		gl = new GameLogic();

		// Black Squares
		for (int x = 0; x < _boardWidth; x++)
			for (int y = 0; y < _boardHeight; y++) {
				if ((x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0)) {
					windows[x][y] = new Window(1);
					getChildren().addAll(windows[x][y]);
				} else {
					windows[x][y] = new Window(0);
					getChildren().addAll(windows[x][y]);
				}
			}

		// Init Rooks
		chessBoard[0][0] = _Rook;
		chessBoard[7][0] = _Rook;
		chessBoard[0][7] = _Rook;
		chessBoard[7][7] = _Rook;
		pieces[0][0] = new PieceRook(PLAYERWHITE);
		pieces[7][0] = new PieceRook(PLAYERWHITE);
		pieces[0][7] = new PieceRook(PLAYERBLACK);
		pieces[7][7] = new PieceRook(PLAYERBLACK);

		// Init Knights

		chessBoard[1][0] = _Knight;
		chessBoard[6][0] = _Knight;
		chessBoard[1][7] = _Knight;
		chessBoard[6][7] = _Knight;
		pieces[1][0] = new PieceKnight(PLAYERWHITE);
		pieces[6][0] = new PieceKnight(PLAYERWHITE);
		pieces[1][7] = new PieceKnight(PLAYERBLACK);
		pieces[6][7] = new PieceKnight(PLAYERBLACK);

		// Init Bishops

		chessBoard[2][0] = _Bishop;
		chessBoard[5][0] = _Bishop;
		chessBoard[2][7] = _Bishop;
		chessBoard[2][7] = _Bishop;
		pieces[2][0] = new PieceBishop(PLAYERWHITE);
		pieces[5][0] = new PieceBishop(PLAYERWHITE);
		pieces[2][7] = new PieceBishop(PLAYERBLACK);
		pieces[5][7] = new PieceBishop(PLAYERBLACK);

		// Init Queens and Kings

		chessBoard[3][0] = _Queen;
		chessBoard[4][0] = _King;
		chessBoard[3][7] = _Queen;
		chessBoard[4][7] = _King;
		pieces[3][0] = new PieceQueen(PLAYERWHITE);
		pieces[4][0] = new PieceKing(PLAYERWHITE);
		pieces[3][7] = new PieceQueen(PLAYERBLACK);
		pieces[4][7] = new PieceKing(PLAYERBLACK);

		// Init Pawns
		for (int x = 0; x <= 7; ++x) {
			chessBoard[x][1] = _Pawn;
			chessBoard[x][6] = _Pawn;
			pieces[x][1] = new PiecePawn(PLAYERWHITE);
			pieces[x][6] = new PiecePawn(PLAYERBLACK);

		}

		// Init Empty
		for (int x = 0; x <= 7; x++) {
			for (int y = 3; y <= 4; ++y) {
				chessBoard[x][y] = _Empty;
				pieces[x][y] = null;
			}

		}

		for (int x = 0; x <= 7; x++) {
			for (int y = 0; y <= 7; ++y) {
				if (pieces[x][y] != null)
					getChildren().add(pieces[x][y]);
			}

		}
		current_player = PLAYERWHITE;
	}

	// resize method
	@Override
	public void resize(double width, double height) {
		// call the superclass method first
		super.resize(width, height);
		// figure out the width and height of a cell
		cell_width = width / _boardWidth;
		cell_height = height / _boardHeight;

		// we need to reset the sizes and positions of all the Pieces that
		// were
		// placed
		for (int x = 0; x < _boardWidth; x++) {
			for (int y = 0; y < _boardHeight; y++) {
				if (windows[x][y] != null) {
					windows[x][y].relocate(x * cell_width, y * cell_height);
					windows[x][y].resize(cell_width, cell_height);
					if (pieces[x][y] != null) {
						pieces[x][y].relocate(x * cell_width, y * cell_height);
						pieces[x][y].resize(cell_width, cell_height);
					}
				}
			}
		}

	}

	// here we handle what happens when something is clicked on the board
	// the behavior change if the board state hasSelected

	public String selectPiece(double x, double y) {
		int indexx = (int) (x / cell_width);
		int indexy = (int) (y / cell_height);
		// if the position is empty then place a piece and swap the players
		if (hasSelected == false) {
			if (pieces[indexx][indexy] != null) {
				if (pieces[indexx][indexy].getColor() == current_player) {
					hasSelected = true;
					selected = new Pair<Integer, Integer>(new Integer(indexx), new Integer(indexy));
					this.colorMoves(indexx, indexy);
				}
			}
		} else if (hasSelected == true) {
			if (gl.canBeMovedTo(indexx, indexy, selected.getKey().intValue(), selected.getValue().intValue(), pieces)) {
				this.movePiece(selected.getKey().intValue(), selected.getValue().intValue(), indexx, indexy);
				hasSelected = false;
				return createStatusString(endOfTurn());
			} else if (pieces[indexx][indexy] != null && pieces[indexx][indexy].getColor() == current_player) {
				resetTiles();
				selected = new Pair<Integer, Integer>(new Integer(indexx), new Integer(indexy));
				this.colorMoves(indexx, indexy);
			}
		}
		return lastStatus;
	}

	private String createStatusString(Boolean[] st) {

		String[] color = { "White", "Black" };

		if (st[0] == true) {
			return lastStatus = color[current_player - 1] + " King in Check";
		} else if (st[1] == true) {
			return lastStatus = color[current_player - 1] + " King in CheckMate press Space to reset";
		} else if (st[2] == true) {
			return lastStatus = "StaleMate press Space to reset";
		}

		return lastStatus;
	}

	// does EOT verifications (Check, CheckMate, Stalemate)
	private Boolean[] endOfTurn() {
		resetTiles();
		current_player = current_player == PLAYERWHITE ? PLAYERBLACK : PLAYERWHITE;

		// the result of this functions will call for a notification;
		Boolean[] tests = new Boolean[3];
		tests[0] = gl.checkCheck(current_player, pieces);
		tests[1] = gl.checkCheckmate(current_player, pieces);
		tests[2] = gl.checkStalemate(current_player, pieces);

		return tests;

	}

	// reset tiles colors
	private void resetTiles() {
		for (int x = 0; x < _boardWidth; ++x) {
			for (int y = 0; y < _boardHeight; ++y) {
				windows[x][y].resetColor();
			}
		}
	}

	// Highlights moves blue for moves, red for agressions/captures
	private void colorMoves(int _x, int _y) {

		this.resetTiles();

		for (int x = 0; x < _boardWidth; ++x) {
			for (int y = 0; y < _boardHeight; ++y) {
				if (gl.canBeMovedTo(x, y, _x, _y, pieces)) {
					if (pieces[x][y] != null && pieces[x][y].color != current_player)
						windows[x][y].turnRed();
					else
						windows[x][y].turnBlue();
				}
			}
		}
	}

	// reset the chessboard this is called on Space.KeyPress event
	public String resetBoard() {

		// color back to normal
		this.resetTiles();
		// unselect piece
		hasSelected = false;

		// nullify chessboard and pieces so GarbageCollector Frees up memory
		// space
		chessBoard = null;
		pieces = null;

		// remove pieces from the parent
		Predicate<? super Node> f = x -> x instanceof Piece;
		getChildren().removeIf(f);

		// re-initalize
		chessBoard = new int[_boardWidth][_boardHeight];
		pieces = new Piece[_boardWidth][_boardHeight];
		// Init Rooks
		chessBoard[0][0] = _Rook;
		chessBoard[7][0] = _Rook;
		chessBoard[0][7] = _Rook;
		chessBoard[7][7] = _Rook;
		pieces[0][0] = new PieceRook(PLAYERWHITE);
		pieces[7][0] = new PieceRook(PLAYERWHITE);
		pieces[0][7] = new PieceRook(PLAYERBLACK);
		pieces[7][7] = new PieceRook(PLAYERBLACK);

		// Init Knights

		chessBoard[1][0] = _Knight;
		chessBoard[6][0] = _Knight;
		chessBoard[1][7] = _Knight;
		chessBoard[6][7] = _Knight;
		pieces[1][0] = new PieceKnight(PLAYERWHITE);
		pieces[6][0] = new PieceKnight(PLAYERWHITE);
		pieces[1][7] = new PieceKnight(PLAYERBLACK);
		pieces[6][7] = new PieceKnight(PLAYERBLACK);

		// Init Bishops

		chessBoard[2][0] = _Bishop;
		chessBoard[5][0] = _Bishop;
		chessBoard[2][7] = _Bishop;
		chessBoard[2][7] = _Bishop;
		pieces[2][0] = new PieceBishop(PLAYERWHITE);
		pieces[5][0] = new PieceBishop(PLAYERWHITE);
		pieces[2][7] = new PieceBishop(PLAYERBLACK);
		pieces[5][7] = new PieceBishop(PLAYERBLACK);

		// Init Queens and Kings

		chessBoard[3][0] = _Queen;
		chessBoard[4][0] = _King;
		chessBoard[3][7] = _Queen;
		chessBoard[4][7] = _King;
		pieces[3][0] = new PieceQueen(PLAYERWHITE);
		pieces[4][0] = new PieceKing(PLAYERWHITE);
		pieces[3][7] = new PieceQueen(PLAYERBLACK);
		pieces[4][7] = new PieceKing(PLAYERBLACK);

		// Init Pawns
		for (int x = 0; x <= 7; ++x) {
			chessBoard[x][1] = _Pawn;
			chessBoard[x][6] = _Pawn;
			pieces[x][1] = new PiecePawn(PLAYERWHITE);
			pieces[x][6] = new PiecePawn(PLAYERBLACK);

		}

		// Init Empty
		for (int x = 0; x <= 7; x++) {
			for (int y = 3; y <= 4; ++y) {
				chessBoard[x][y] = _Empty;
				pieces[x][y] = null;
			}

		}

		for (int x = 0; x <= 7; x++) {
			for (int y = 0; y <= 7; ++y) {
				if (pieces[x][y] != null)
					getChildren().add(pieces[x][y]);
			}

		}
		current_player = PLAYERWHITE;
		return lastStatus;
	}

	// move piece method
	private void movePiece(int s_x, int s_y, int e_x, int e_y) {

		// if piece on dest, remove piece from array and from parent/display
		if (pieces[e_x][e_y] != null)
			getChildren().remove(pieces[e_x][e_y]);

		// pawn promotion goes here
		if (pieces[s_x][s_y].getType() == _Pawn && (e_y == 0 || e_y == 7)) {
			getChildren().remove(pieces[s_x][s_y]);
			pieces[e_x][e_y] = new PieceQueen(current_player);
			chessBoard[e_x][e_y] = _Queen;
			pieces[s_x][s_y] = null;
			chessBoard[s_x][s_y] = _Empty;
			getChildren().add(pieces[e_x][e_y]);
		} else { // move piece in the array
			pieces[e_x][e_y] = pieces[s_x][s_y];
			chessBoard[e_x][e_y] = chessBoard[s_x][s_y];
			pieces[s_x][s_y] = null;
			chessBoard[s_x][s_y] = _Empty;
		}
		// move piece on display
		pieces[e_x][e_y].relocate(e_x * cell_height, e_y * cell_width);
		pieces[e_x][e_y].resize(cell_height, cell_width);
	}

	// test functions resets the board and place pieces in condition to test
	// different outcomes
	public String testCheckMate() {

		// color back to normal
		this.resetTiles();
		// unselect piece
		hasSelected = false;

		// nullify chessboard and pieces so GarbageCollector Frees up memory
		// space
		chessBoard = null;
		pieces = null;

		// remove pieces from the parent
		Predicate<? super Node> f = x -> x instanceof Piece;
		getChildren().removeIf(f);

		// re-initalize
		chessBoard = new int[_boardWidth][_boardHeight];
		pieces = new Piece[_boardWidth][_boardHeight];

		// Init Empty
		for (int x = 0; x <= 7; x++) {
			for (int y = 0; y <= 7; ++y) {
				chessBoard[x][y] = _Empty;
				pieces[x][y] = null;
			}

		}

		// Init Rooks
		chessBoard[7][6] = _Rook;
		chessBoard[7][7] = _Rook;
		pieces[7][6] = new PieceRook(PLAYERWHITE);
		pieces[7][7] = new PieceRook(PLAYERWHITE);

		// Init Queens and Kings

		chessBoard[7][0] = _King;
		chessBoard[4][7] = _King;
		pieces[4][0] = new PieceKing(PLAYERWHITE);
		pieces[4][7] = new PieceKing(PLAYERBLACK);

		for (int x = 0; x <= 7; x++) {
			for (int y = 0; y <= 7; ++y) {
				if (pieces[x][y] != null)
					getChildren().add(pieces[x][y]);
			}

		}
		current_player = PLAYERWHITE;
		return createStatusString(endOfTurn());
	}

	public String testStaleMate() {

		// color back to normal
		this.resetTiles();
		// unselect piece
		hasSelected = false;

		// nullify chessboard and pieces so GarbageCollector Frees up memory
		// space
		chessBoard = null;
		pieces = null;

		// remove pieces from the parent
		Predicate<? super Node> f = x -> x instanceof Piece;
		getChildren().removeIf(f);

		// re-initalize
		chessBoard = new int[_boardWidth][_boardHeight];
		pieces = new Piece[_boardWidth][_boardHeight];

		for (int x = 0; x <= 7; x++) {
			for (int y = 0; y <= 7; ++y) {
				chessBoard[x][y] = _Empty;
				pieces[x][y] = null;
			}
		}

		// Init Rooks
		chessBoard[5][5] = _Queen;
		chessBoard[5][7] = _Queen;
		pieces[5][5] = new PieceQueen(PLAYERWHITE);
		pieces[5][7] = new PieceQueen(PLAYERWHITE);

		// Init Queens and Kings

		chessBoard[7][0] = _King;
		chessBoard[7][6] = _King;
		pieces[4][0] = new PieceKing(PLAYERWHITE);
		pieces[7][6] = new PieceKing(PLAYERBLACK);

		for (int x = 0; x <= 7; x++) {
			for (int y = 0; y <= 7; ++y) {
				if (pieces[x][y] != null)
					getChildren().add(pieces[x][y]);
			}

		}

		current_player = PLAYERWHITE;
		return createStatusString(endOfTurn());
	}

}

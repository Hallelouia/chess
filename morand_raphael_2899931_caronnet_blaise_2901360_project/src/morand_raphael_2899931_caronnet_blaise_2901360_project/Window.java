package morand_raphael_2899931_caronnet_blaise_2901360_project;

//imports required for this class
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Translate;

// class definition 
class Window extends Group {

	// private fields of the class
	private Rectangle e; // ellipse for rendering this window
	private Translate pos; // translate to set the position of this window
	private int color;

	// constructor for the class
	public Window(int i) {
		// create a new translate object and take a copy of the type
		pos = new Translate();

		e = new Rectangle();
		getChildren().addAll(e);
		e.getTransforms().add(pos);
		color = i;

		if (color == 1) {
			e.setFill(Color.GREY);
		} else
			e.setFill(Color.WHITE);
	}

	// overridden version of the resize method
	@Override
	public void resize(double width, double height) {
		// call the super class method
		super.resize(width, height);
		e.setWidth(width);
		e.setHeight(height);
	}

	// overridden version of the relocate method
	@Override
	public void relocate(double x, double y) {
		// call the superclass method and update the position
		super.relocate(x, y);
		pos.setX(x);
		pos.setY(y);
	}

	public void resetColor() {
		if (color == 1) {
			e.setFill(Color.GREY);
		} else
			e.setFill(Color.WHITE);
	}

	public void turnBlue() {
		if (color == 1) {
			e.setFill(Color.STEELBLUE);
		} else
			e.setFill(Color.SKYBLUE);
	}

	public void turnRed() {
		if (color == 1) {
			e.setFill(Color.DARKRED);
		} else
			e.setFill(Color.INDIANRED);
	}

}
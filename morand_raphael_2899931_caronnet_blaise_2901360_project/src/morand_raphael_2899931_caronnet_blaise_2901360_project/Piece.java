package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.Group;
import javafx.scene.image.ImageView;

//class declaration - abstract because we will not want to create a Piece object but we would
//like to specify the private fields that all pieces should have in addition to their behaviours
public abstract class Piece extends Group {

	// piece can be either white (1) or black (2)
	protected int color;
	// image used to display the piece
	protected ImageView image;

	protected final int _Empty = 0;
	protected final int _Pawn = 1;
	protected final int _Knight = 2;
	protected final int _Bishop = 3;
	protected final int _Rook = 4;
	protected final int _Queen = 5;
	protected final int _King = 6;

	public Piece(int type) {
		this.color = type;
	}

	public int getColor() {
		return color;
	}

	public int getType() {
		return _Empty;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {

		System.out.println("coucou !");

		return false;
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		if (image != null) {
			image.setFitWidth(width);
			image.setFitHeight(height);
		}
	}

	// overridden version of the relocate method
	@Override
	public void relocate(double x, double y) {
		super.relocate(x, y);
		if (image != null) {
			image.relocate(x, y);
		}
	}

	// move method

	// is captured method

}

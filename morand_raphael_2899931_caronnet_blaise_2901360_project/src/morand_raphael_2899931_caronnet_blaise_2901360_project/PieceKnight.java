package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.image.ImageView;

public class PieceKnight extends Piece {
	public PieceKnight(int type) {
		super(type);
		if (type == 1) {
			image = new ImageView("chess-pieces/W-Knight.png");
			getChildren().addAll(image);
		} else {
			image = new ImageView("chess-pieces/B-Knight.png");
			getChildren().addAll(image);
		}
	}

	public int getType() {
		return _Knight;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		if (_pieces[_x][_y] != null && _pieces[_x][_y].getColor() == _pieces[_pos_x][_pos_y].getColor())
			return false;

		int diff_x = _x - _pos_x;
		int diff_y = _y - _pos_y;

		if (((diff_x == 2 || diff_x == -2) && (diff_y == 1 || diff_y == -1))
				|| ((diff_y == 2 || diff_y == -2) && (diff_x == 1 || diff_x == -1)))
			return true;

		return false;
	}
}

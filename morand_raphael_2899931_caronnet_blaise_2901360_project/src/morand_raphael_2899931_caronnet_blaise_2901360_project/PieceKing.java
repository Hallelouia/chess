package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.image.ImageView;

public class PieceKing extends Piece {

	public PieceKing(int type) {
		super(type);
		if (type == 1) {
			image = new ImageView("chess-pieces/W-King.png");
			getChildren().addAll(image);
		} else {
			image = new ImageView("chess-pieces/B-King.png");
			getChildren().addAll(image);
		}
	}

	public int getType() {
		return _King;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		if (_pieces[_x][_y] != null && _pieces[_x][_y].getColor() == _pieces[_pos_x][_pos_y].getColor())
			return false;

		if (_x <= _pos_x + 1 && _x >= _pos_x - 1 && _y <= _pos_y + 1 && _y >= _pos_y - 1)
			return true;
		return false;
	}

}

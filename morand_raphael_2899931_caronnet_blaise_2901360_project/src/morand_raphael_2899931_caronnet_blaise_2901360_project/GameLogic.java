package morand_raphael_2899931_caronnet_blaise_2901360_project;

public class GameLogic {
	private final int _Empty = 0;
	private final int _Pawn = 1;
	private final int _Knight = 2;
	private final int _Bishop = 3;
	private final int _Rook = 4;
	private final int _Queen = 5;
	private final int _King = 6;

	public void printBoard(Piece _pieces[][]) {
		for (int i_x = 0; i_x < 8; i_x++)
			System.out.print("-");

		for (int i_y = 0; i_y < 8; i_y++) {
			System.out.println("");
			for (int i_x = 0; i_x < 8; i_x++) {
				if (_pieces[i_x][i_y] != null) {
					if (_pieces[i_x][i_y].getColor() == 1) {
						if (_pieces[i_x][i_y].getType() == _Pawn)
							System.out.print("P");
						else if (_pieces[i_x][i_y].getType() == _Knight)
							System.out.print("H");
						else if (_pieces[i_x][i_y].getType() == _Bishop)
							System.out.print("B");
						else if (_pieces[i_x][i_y].getType() == _Rook)
							System.out.print("R");
						else if (_pieces[i_x][i_y].getType() == _Queen)
							System.out.print("Q");
						else if (_pieces[i_x][i_y].getType() == _King)
							System.out.print("K");
					} else {
						if (_pieces[i_x][i_y].getType() == _Pawn)
							System.out.print("1");
						else if (_pieces[i_x][i_y].getType() == _Knight)
							System.out.print("2");
						else if (_pieces[i_x][i_y].getType() == _Bishop)
							System.out.print("3");
						else if (_pieces[i_x][i_y].getType() == _Rook)
							System.out.print("4");
						else if (_pieces[i_x][i_y].getType() == _Queen)
							System.out.print("5");
						else if (_pieces[i_x][i_y].getType() == _King)
							System.out.print("6");
					}
				} else
					System.out.print(" ");
			}
		}
		System.out.println("");
	}

	// method to check if a square is safe
	public Boolean isSafe(int _color, Piece _pieces[][], int _x, int _y) {
		for (int i_x = 0; i_x < 8; i_x++) {
			for (int i_y = 0; i_y < 8; i_y++) {
				if (_pieces[i_x][i_y] != null && _pieces[i_x][i_y].getColor() != _color) {
					if (_pieces[i_x][i_y].isInVision(_x, _y, i_x, i_y, _pieces))
						return false;
				}
			}
		}

		return true;
	}

	// method to check if a king is secure
	public Boolean kingIsSafe(int _color, Piece _pieces[][]) {
		int kingPosX = 0;
		int kingPosY = 0;

		// Find king position
		for (int i_y = 0; i_y < 8; i_y++) {
			for (int i_x = 0; i_x < 8; i_x++) {
				if (_pieces[i_x][i_y] != null && _pieces[i_x][i_y].getType() == _King
						&& _pieces[i_x][i_y].getColor() == _color) {
					kingPosX = i_x;
					kingPosY = i_y;
				}
			}
		}

		// Verify if king is in security
		for (int i_x = 0; i_x < 8; i_x++) {
			for (int i_y = 0; i_y < 8; i_y++) {
				if (_pieces[i_x][i_y] != null && _pieces[i_x][i_y].getColor() != _color
						&& _pieces[i_x][i_y].isInVision(kingPosX, kingPosY, i_x, i_y, _pieces)) {
					return false;
				}
			}
		}

		return true;
	}

	public Boolean canBeMovedTo(int _dest_x, int _dest_y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		int color = _pieces[_pos_x][_pos_y].getColor();
		Piece buff[][] = new Piece[8][8];

		if (_pieces[_pos_x][_pos_y].isInVision(_dest_x, _dest_y, _pos_x, _pos_y, _pieces)) {
			// Check if the king will be safe
			for (int buff_x = 0; buff_x < 8; buff_x++) {
				for (int buff_y = 0; buff_y < 8; buff_y++) {
					buff[buff_x][buff_y] = _pieces[buff_x][buff_y];
				}
			}

			buff[_dest_x][_dest_y] = buff[_pos_x][_pos_y];
			buff[_pos_x][_pos_y] = null;

			if (kingIsSafe(color, buff))
				return true;
		}

		return false;
	}

	public Boolean pieceIsStuck(Piece _pieces[][], int _x, int _y) {
		int color = _pieces[_x][_y].getColor();

		for (int i_x = 0; i_x < 8; i_x++) {
			for (int i_y = 0; i_y < 8; i_y++) {
				if ((i_x != _x || i_y != _y) && (_pieces[i_x][i_y] == null
						|| (_pieces[i_x][i_y] != null && _pieces[_x][_y].getColor() != color))) {
					if (canBeMovedTo(i_x, i_y, _x, _y, _pieces))
						return false;
				}
			}
		}

		return true;
	}

	public Boolean gameIsOver(int _color, Piece _pieces[][]) {
		for (int i_x = 0; i_x < 8; i_x++) {
			for (int i_y = 0; i_y < 8; i_y++) {
				if (_pieces[i_x][i_y] != null && _pieces[i_x][i_y].getColor() == _color
						&& !pieceIsStuck(_pieces, i_x, i_y))
					return false;
			}
		}

		return true;
	}

	// method to detect stalemate
	public Boolean checkStalemate(int _color, Piece _pieces[][]) {
		if (kingIsSafe(_color, _pieces) && gameIsOver(_color, _pieces))
			return true;
		return false;
	}

	// method to check checkmate
	public Boolean checkCheckmate(int _color, Piece _pieces[][]) {
		if (!kingIsSafe(_color, _pieces) && gameIsOver(_color, _pieces))
			return true;
		return false;
	}

	// method to detect check for notification (subject driven method)
	public Boolean checkCheck(int _color, Piece _pieces[][]) {
		if (!kingIsSafe(_color, _pieces) && !gameIsOver(_color, _pieces))
			return true;
		return false;
	}
}

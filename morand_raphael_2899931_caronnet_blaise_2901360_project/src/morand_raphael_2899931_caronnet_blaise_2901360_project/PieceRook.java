package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.image.ImageView;

public class PieceRook extends Piece {

	public PieceRook(int type) {
		super(type);
		if (type == 1) {
			image = new ImageView("chess-pieces/W-Rook.png");
			getChildren().addAll(image);
		} else {
			image = new ImageView("chess-pieces/B-Rook.png");
			getChildren().addAll(image);
		}
	}

	public int getType() {
		return _Rook;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		if (_pieces[_x][_y] != null && _pieces[_x][_y].getColor() == _pieces[_pos_x][_pos_y].getColor())
			return false;

		if (_x == _pos_x) {
			int yMin;
			int yMax;

			if (_y > _pos_y) {
				yMax = _y;
				yMin = _pos_y;
			} else {
				yMax = _pos_y;
				yMin = _y;
			}

			for (int i_y = yMin + 1; i_y < yMax; i_y++) {
				if (_pieces[_x][i_y] != null)
					return false;
			}

			return true;
		} else if (_y == _pos_y) {
			int xMin;
			int xMax;

			if (_x > _pos_x) {
				xMax = _x;
				xMin = _pos_x;
			} else {
				xMax = _pos_x;
				xMin = _x;
			}

			for (int i_x = xMin + 1; i_x < xMax; i_x++) {
				if (_pieces[i_x][_y] != null)
					return false;
			}

			return true;
		}

		return false;
	}
}

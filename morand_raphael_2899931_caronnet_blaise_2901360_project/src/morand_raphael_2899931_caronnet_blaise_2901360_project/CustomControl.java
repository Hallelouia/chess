package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

public class CustomControl extends Control {

	// private fields of the class

	private ChessBoard chessBoard; // a Connect 4 board
	private TextField status;
	private GridPane grid;

	// similar to previous custom controlls but must handle more
	// complex mouse interactions and key interactions
	// constructor for the class
	public CustomControl() {
		// set a default skin and generate a game board
		setSkin(new CustomControlSkin(this));
		chessBoard = new ChessBoard();
		status = new TextField();
		grid = new GridPane();
		status.setEditable(false);
		grid.addRow(0, chessBoard);
		grid.addRow(1, status);

		getChildren().add(grid);

		// add a mouse clicked listener that will try to place a piece
		setOnMouseClicked(new EventHandler<MouseEvent>() {
			// overridden handle method
			@Override
			public void handle(MouseEvent event) {
				status.setText(chessBoard.selectPiece(event.getX(), event.getY()));
			}
		});
		setOnKeyPressed(new EventHandler<KeyEvent>() {
			// overridden handle method
			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.SPACE)
					status.setText(chessBoard.resetBoard());
				if (event.getCode() == KeyCode.DIGIT1)
					status.setText(chessBoard.testCheckMate());
				if (event.getCode() == KeyCode.DIGIT2)
					status.setText(chessBoard.testStaleMate());
			}
		});

	}

	// override the resize method
	@Override
	public void resize(double width, double height) {
		// update the size of the rectangle
		super.resize(width, height);
		chessBoard.resize(width, height);
	}
}

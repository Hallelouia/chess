package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class StatusBar extends Pane {
	private TextField tf;

	public StatusBar() {
		tf = new TextField();
		tf.setEditable(false);
	}

	public void update(String status) {
		tf.setText(status);
	}
}

package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.image.ImageView;

public class PiecePawn extends Piece {

	public PiecePawn(int type) {
		super(type);
		if (type == 1) {
			image = new ImageView("chess-pieces/W-Pawn.png");
			getChildren().addAll(image);
		} else {
			image = new ImageView("chess-pieces/B-Pawn.png");
			getChildren().addAll(image);
		}
	}

	public int getType() {
		return _Pawn;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		if (color == 2) {
			// BLACK
			if (_y == _pos_y - 1) {
				if ((_x == _pos_x - 1 || _x == _pos_x + 1) && _pieces[_x][_y] != null
						&& _pieces[_x][_y].getColor() != color)
					return true;
				else if (_x == _pos_x && _pieces[_x][_y] == null)
					return true;
			}
			if (_pos_y == 6 && _y == 4 && _x == _pos_x && _pieces[_x][5] == null && _pieces[_x][4] == null)
				return true;
		} else if (color == 1) {
			// WHITE
			if (_y == _pos_y + 1) {
				if ((_x == _pos_x - 1 || _x == _pos_x + 1) && _pieces[_x][_y] != null
						&& _pieces[_x][_y].getColor() != color)
					return true;
				else if (_x == _pos_x && _pieces[_x][_y] == null)
					return true;
			}
			if (_pos_y == 1 && _y == 3 && _x == _pos_x && _pieces[_x][2] == null && _pieces[_x][3] == null)
				return true;
		}

		return false;
	}

}

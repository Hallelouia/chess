package morand_raphael_2899931_caronnet_blaise_2901360_project;

import javafx.scene.image.ImageView;

public class PieceBishop extends Piece {

	public PieceBishop(int type) {
		super(type);
		if (type == 1) {
			image = new ImageView("chess-pieces/W-Bishop.png");
			getChildren().addAll(image);
		} else {
			image = new ImageView("chess-pieces/B-Bishop.png");
			getChildren().addAll(image);
		}
	}

	public int getType() {
		return _Bishop;
	}

	public Boolean isInVision(int _x, int _y, int _pos_x, int _pos_y, Piece _pieces[][]) {
		if (_pieces[_x][_y] != null && _pieces[_x][_y].getColor() == _pieces[_pos_x][_pos_y].getColor())
			return false;

		int diff_x = _pos_x - _x;
		int diff_y = _pos_y - _y;

		if (diff_x + diff_y != 0 && diff_x - diff_y != 0)
			return false;

		int it_x;
		int it_y;

		if (_x > _pos_x)
			it_x = 1;
		else
			it_x = -1;

		if (_y > _pos_y)
			it_y = 1;
		else
			it_y = -1;

		int x = _pos_x + it_x;
		int y = _pos_y + it_y;

		while (x != _x) {
			if (_pieces[x][y] != null)
				return false;

			x += it_x;
			y += it_y;
		}

		return true;
	}
}
